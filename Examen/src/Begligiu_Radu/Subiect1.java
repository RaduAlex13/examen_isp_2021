package Begligiu_Radu;

import java.util.LinkedList;
import java.util.List;

public class Subiect1 {

    public static void main(String[] args) {

    }
}

class Account {

    private List<Card> cards = new LinkedList<>();

    public void addCard(Card card) {
        cards.add((card));
    }
}

class Bank {

    private List<Account> accounts = new LinkedList<>();
    private User user;

    public Bank(User user) {
        this.user = user;
    }

    public void addAccount(Account account) {
        accounts.add(account);
    }
}

class Transaction {

    private int amount;
    private Account senderAccount;
    private Account receiverAccount;

    public Transaction(int amount, Account senderAccount, Account receiverAccount) {
        this.amount = amount;
        this.senderAccount = senderAccount;
        this.receiverAccount = receiverAccount;
    }
}

class Card {

    private int sum = 0;

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }
}

class User {

    private Account account;

    public User(Account account) {
        this.account = account;
    }
}