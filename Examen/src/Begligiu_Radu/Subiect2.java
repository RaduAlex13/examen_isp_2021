package Begligiu_Radu;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class Frame extends JFrame{
    JTextField t1=new JTextField();
    JTextField t2=new JTextField();
    JTextField t3=new JTextField();
    JButton b=new JButton();
    String data="abcd";
    int click=0;
    Frame(String title){
        super(title);
        this.setSize(300,300);
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLayout(null);
        createGUI();
    }
    public void createGUI(){
        t1.setBounds(20,30,100,30);
        t2.setBounds(20,60,100,30);
        t3.setBounds(20,90,100,30);
        t3.setEditable(false);
        b.setBounds(20,120,30,30);
        add(t1);
        add(t2);
        add(t3);
        add(b);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int a=Integer.parseInt(t1.getText());
                int b=Integer.parseInt(t2.getText());
                int c=a+b;
                t3.setText(String.valueOf(c));
            }
        });
    }
}
public class Subiect2 {
    public static void main(String[] args){
        new Frame("MyFrame");
    }
}

